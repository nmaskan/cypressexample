/// <reference types="cypress" />
describe("Inspect Automation Test Sttore items using chain of command ", ()=>{
    it('click of the first item using item header', () => {
        cy.visit("https://www.automationteststore.com", {timeout: 200000});
        cy.get('#block_frame_featured_1769 > .thumbnails > :nth-child(1) > .fixed_wrapper > .fixed > .prdocutname').click();
         
    });

    it.only('Click on the first item using item text', () => {
        cy.visit("https://www.automationteststore.com", {timeout: 200000});
        cy.get('.prdocutname').contains('Skinsheen Bronzer Stick').click().then(function(itemHeaderText) {
            console.log("Selected the following item: "+ itemHeaderText.text())
        });

    });

    it('Click on the first item using index', () => {
        cy.visit("https://www.automationteststore.com", {timeout: 200000});
        cy.get('.fixed_wrapper').find('.prdocutname').eq(0).click();

    });

    

});