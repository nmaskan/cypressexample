/// <reference types="cypress" />

describe("Verifying variables, cypress commands and jquery commands", ()=>{

    it('Navigating to specific products pages', () => {
        cy.visit("https://www.automationteststore.com", {timeout: 200000});
        //The following will fail
        // const makeupLink = cy.get("a[href*='product/category&path=']").contains("Makeup")
        // const skincareLink = cy.get("a[href*='product/category&path=']").contains("Skincare")
        // makeupLink.click();
        // skincareLink.click();

        //The following will pass
        // const makeupLink = cy.get("a[href*='product/category&path=']").contains("Makeup")
        // makeupLink.click();
        // const skincareLink = cy.get("a[href*='product/category&path=']").contains("Skincare")
        // skincareLink.click();

        //Recomended Approach
        cy.get("a[href*='product/category&path=']").contains("Makeup").click()
        cy.get("a[href*='product/category&path=']").contains("Skincare").click()
    });

    it('Navigating to specific products pages', () => {
        cy.visit("https://www.automationteststore.com", {timeout: 200000});
        cy.get("a[href*='product/category&path=']").contains("Makeup").click()

        //Following code will fail
        // const header = cy.get("h1 .maintext");
        // cy.log(header.text());

        cy.get("h1 .maintext").then(($headerText)=>{
            const headerText = $headerText.text()
            cy.log("Found header text: " + headerText)
            expect(headerText).is.eq('Makeup')
        })
        
    });

    it.only('Validate properties of the Contact page', () => {
        cy.visit("https://automationteststore.com/index.php?rt=content/contact", {timeout: 200000});

        //Uses cypress commands and chaining
        cy.contains('#ContactUsFrm', 'Contact Us Form').find('#field_11').should('contain', 'First name')

        //JQuery Approach
        cy.contains('#ContactUsFrm', 'Contact Us Form').then(text =>{
            const firstNameText = text.find('#field_11').text()
            expect(firstNameText).to.contain('First name')

            //Embeded commands (Closure)
            cy.get('#field_11').then(finText => {
                cy.log(finText.text())
                cy.log(finText)
            })

        })
        
    });

});