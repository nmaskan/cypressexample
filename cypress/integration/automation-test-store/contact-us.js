/// <reference types="cypress" />
describe("Test Contact Us form via Automation Test Stor", ()=>{
    it('should be able to submit a successful submission via contact us form', () => {
        cy.visit("https://www.automationteststore.com", {timeout: 200000});
        cy.document().should('have.attr', 'charset').and('eq', 'UTF-8');
        cy.get("a[href$='contact']").click().then(function(linkText){
            cy.log("Clicked on link usin text: " + linkText.text())
        });
        //cy.xpath("//a[contains(@href, 'contact']").click()
        cy.get('#ContactUsFrm_first_name').type("jamal");
        cy.get('#ContactUsFrm_email').type("jamal_jabir23@gmail.com");
        cy.get('#ContactUsFrm_email').should('have.attr', 'name','email');
        cy.get('#ContactUsFrm_enquiry').type("do you provide additional discount on bulk orders?");
        cy.get("button[title='Submit']").click();
        cy.get('.mb40 > :nth-child(3)').should('have.text', 'Your enquiry has been successfully sent to the store owner!');
        cy.log('Test is completed');
    });

});