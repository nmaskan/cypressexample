const { should } = require("chai")

describe('Browser action', () => {

    it('should load Questico website ', () => {
        cy.visit('https://qa-www.questico.de/?platform=nf', {timeout: 200000})
        cy.url().should('include', '/?platform=nf')
    })

    it('should show banner Nur fuer dich:erstgespräch gratis!', () =>{
        cy.get('.jss8').should('be.visible')
          .get('.jss10 > .MuiTypography-root').should('be.visible')
                                              .should('contain.text', "Nur für dich: Erstgespräch gratis!")
    })

    it('should show deine-berater-finden page',() =>{
        cy.get('.jss10 > .MuiTypography-root').click()
        cy.url().should('include', 'deinen-berater-finden/start')
        cy.get('h2').should('contain.text',"Was beschäftigt dich momentan?")
    })

    it('should display homepage content', () =>{
        cy.get('.jss849 > .MuiTypography-root > svg').click()
        cy.get('h1').should('have.text', 'Deine Antworten findest du mit Questico')
    })

    it('should show the correct number of expert in default pagination list', () =>{
        cy.get('ul li')
            .its('length')
            .should('be.gt',4)
    })


    /*it('should wait 3 seconds', () => {
        cy.wait(3000)
    })

    it('should pause the execution', () => {
        cy.pause()
    })

    it('should check for correct element on the page', () => {
        cy.get('button').should('be.visible')
    })*/

})
