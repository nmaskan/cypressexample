///<reference types="Cypress" />

describe('reigistration new user',()=> {

    it('should load Questico website ', () => {
        cy.visit('https://qa-www.questico.de/?platform=nf', {timeout: 200000})
        cy.clearCookies({domain:Cypress.env("KCMS_URL")},{log:true})
        cy.clearLocalStorage(Cypress.env("KCMS_URL"),{log:true})
        cy.url().should('include', '/?platform=nf')
    })

    it('should display correct number of expert default list', () =>{
        cy.get('ul li')
            .its('length')
            .should('be.gt',4)
    })


    it('should click on Registrieren Button', () => {
        cy.get('button').contains('Registrieren').click()
          .wait(10000)
        //cy.get('h2').contains('Jetzt registrieren und kostenloses Erstgespräch nutzen!')
        //cy.get('input').should('be.visible')
    })

    it('should fill invalid email address', () => {
        cy.get('iframe').iframe(() => {
            // Targets the input within the iframe element
            cy.get('#email').clear()
            cy.get('#email').type('fakeqemail.com')
          })
        
    })

    it('should fill invalid password', ()=> {
        cy.get('iframe').iframe(()=>{
            cy.get('#password').clear()
            cy.get('#password').type('atla')

        })
    })

    it('should submit Registrieren form', ()=> {
        cy.get('iframe').iframe(()=>{
            cy.get('form').submit()

        })
        
    })

    it('should display error message', ()=>{
        cy.get('iframe').iframe(()=>{
            cy.get('.Input_errorHint__X6GnZ').should('be.visible')

        })
        
    })

    it('should fill with already existing email address', () => {
        cy.get('iframe').iframe(() => {
            cy.frameLoaded('iframe')
            // Targets the input within the iframe element
            cy.get('#email').clear()
            cy.get('#email').type('morkosch@live.de')
          })
        
    })

    it('should fill valid password', ()=> {
        cy.get('iframe').iframe(()=>{
            cy.get('#password').clear()
            cy.get('#password').type('atlas3ma')

        })
    })

    it('should submit Registrieren form', ()=> {
        cy.get('iframe').iframe(()=>{
            cy.get('form').submit()

        })
        
    })

    it('should display alert message', ()=>{
        cy.get('iframe').iframe(()=>{
            cy.get('.Message_alert__nk5eo Message_error__1WWtk').should('be.visible')
            cy.get('.Message_alert__nk5eo Message_error__1WWtk').should('have.text', "Bitte gib eine gültige E-Mail-Adresse ein.")

        })
        
    })
})